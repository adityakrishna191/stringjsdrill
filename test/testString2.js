const problem2function = require("../string2.js");

const result1 = problem2function("111.139.161.143");

const result2 = problem2function("2001:0db8:85a3:0000:0000:8a2e:0370:7334") //example of ipv6 which should return []

console.log(result1);