const problem4function = require("../string4.js");

const result1 = problem4function({"first_name": "JoHN", "last_name": "SMith"})

const result2 = problem4function({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"});

console.log(result2);