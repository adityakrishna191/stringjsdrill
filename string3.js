function monthFromDate(string) {
    // console.log(string.split("/"));
    const dateArray = string.split("/");
  
    if(Number(dateArray[0])<1 || Number(dateArray[0])>31) {
      return "Invalid Date";
    }
    
    if(Number(dateArray[1])==1) {
      return "January";
    }else if(Number(dateArray[1])==2) {
      return "February";
    }else if(Number(dateArray[1])==3) {
      return "March";
    }else if(Number(dateArray[1])==4) {
      return "April";
    }else if(Number(dateArray[1])==5) {
      return "May";
    }else if(Number(dateArray[1])==6) {
      return "June";
    }else if(Number(dateArray[1])==7) {
      return "July";
    }else if(Number(dateArray[1])==8) {
      return "August";
    }else if(Number(dateArray[1])==9) {
      return "September";
    }else if(Number(dateArray[1])==10) {
      return "October";
    }else if(Number(dateArray[1])==11) {
      return "November";
    }else if(Number(dateArray[1])==12) {
      return "December";
    }else{
      return "Invalid Date";
    }
}

module.exports = monthFromDate;
  
// const result = monthFromDate("10/1/2021");

// console.log(result);