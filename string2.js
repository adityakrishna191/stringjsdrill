function ipv4String2Array(string) {
    // console.log(string.split("."));
    const octetsArray = string.split(".");
    
    if (octetsArray.length!==4) {
      return [];
    }
    
    //ipv4 should only contain 4 octets and numbers should lie in [0,255]
    
    const octetsArrayInNumber = [];
    
    for (let i = 0; i < octetsArray.length; i++) {
      if (Number(octetsArray[i])<0 || Number(octetsArray[i])>255 || Number(octetsArray[i])===NaN) {
        return [];
      }else{
        octetsArrayInNumber.push(Number(octetsArray[i]));
      }
    }
  
    return octetsArrayInNumber;
}

module.exports = ipv4String2Array;

// const result1 = ipv4String2Array("111.139.161.143");

// const result2 = ipv4String2Array("2001:0db8:85a3:0000:0000:8a2e:0370:7334") //example of ipv6 which should return []

// console.log(result1);