function convertArray2Strings(array) {
    if(array.length === 0){
      return "";
    }
    const joinedStrings = array.join(" ");
    const period = ".";
    const result = joinedStrings + period;
    return result;
}

module.exports = convertArray2Strings;

//   const result = convertArray2Strings(["the", "quick", "brown", "fox"])
  
//   const result2 = convertArray2Strings([]);
  
//   console.log(result);