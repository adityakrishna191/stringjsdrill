function nameInTitleCase(object) {
    // console.log(object);
    const nameElements = Object.values(object);
    // console.log(nameElements);
    const nameArr = []
    for (let i = 0; i < nameElements.length; i++) {
        const smallChar = nameElements[i].slice(1).toLowerCase();
        const firstChar = nameElements[i][0].toUpperCase();
        // console.log(firstChar + smallChar);
        const fullName = firstChar + smallChar
        
        nameArr.push(fullName);
    }
    
    // console.log(nameArr);
    return nameArr.join(" ");
}
  
module.exports = nameInTitleCase;

// const result1 = nameInTitleCase({"first_name": "JoHN", "last_name": "SMith"})

// const result2 = nameInTitleCase({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"});

// console.log(result2);