function string2Number (string) {
    const eachElementsArray = string.split("");
    //remove ","
    for(let i=0;i<eachElementsArray.length;) {
      if(eachElementsArray[i]===","){
        eachElementsArray.splice(i,1);
      }else{
        i++;
      }
    }
    // console.log(eachElementsArray);
  
    string = eachElementsArray.join(""); //string without comma
    
    //check for number of "-","$","."
    let countMinus = 0;
    let countDollar = 0;
    let countPeriod = 0;
  
    for( let i = 0; i < string.length; i++) {
      const char = string.charAt(i); //console.log(char);
      if (char==="-") {
        countMinus++;
      }else if (char==="$") {
        countDollar++;
      }else if (char===".") {
        countPeriod++;
      }
    }
  
    if (countMinus>1 || countDollar>1 || countPeriod>1) {
      return 0;
    }
  
    let indexOfDollar = eachElementsArray.indexOf("$");
  
    let indexOfMinus = eachElementsArray.indexOf("-");
  
    let indexOfPeriod = eachElementsArray.indexOf(".");
  
    if(indexOfMinus===-1){ //check if number is positive
      if(indexOfDollar!==0){ //check if Dollar sign is misplaced, it should be at the beginning of string
        return 0; 
      }else{
        const onlyNumbers = string.slice(1);
        return Number(onlyNumbers);
      }
    }
    
    if(indexOfMinus!==0 && indexOfDollar!==1){ //if number is negative "-" should be at the beginning of string and immediately followed by "$"
      return 0;
    }
    
    if(indexOfMinus===0){ //for negative numbers
      const onlyNumbers = string.slice(2);
      return Number("-"+onlyNumbers);
    }
    
}
  
module.exports = string2Number;

// const result1 = string2Number("$1,002.22");
// const result2 = string2Number("-$1,002.245");
// console.log(result2);